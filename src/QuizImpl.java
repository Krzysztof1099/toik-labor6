public class QuizImpl implements Quiz {

    private int digit;

    public QuizImpl() {
        this.digit = 254;	// zmienna moze ulegac zmianie!
    }

    public void isCorrectValue(int value)
            throws Quiz.ParamTooLarge, Quiz.ParamTooSmall {


        if (value > digit) {
            throw new ParamTooLarge();
        } else if (value < digit) {
            throw new ParamTooSmall();
        }
        else{
            System.out.println("Bingo!");
        }
    }

}




